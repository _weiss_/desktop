# .zshenv

# ENVIRONMENT VARIABLES ----------------------------------------------------------------------------

export ZDOTDIR=$HOME/.config/zsh

export EDITOR=nvim
export VISUAL=nvim

export MOZ_ENABLE_WAYLAND=1

# EOF ----------------------------------------------------------------------------------------------
