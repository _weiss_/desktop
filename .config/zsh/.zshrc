# .zshrc

# BASICS -------------------------------------------------------------------------------------------

HISTFILE=~/.config/zsh/.histfile
HISTSIZE=256
SAVEHIST=256

bindkey -v

zstyle :compinstall filename '/home/libertas/.config/zsh/.zshrc'

autoload -Uz compinit
compinit

# ALIASES ------------------------------------------------------------------------------------------

alias rm='rm -i'

# FUNCTIONS ----------------------------------------------------------------------------------------

ctest() {
    [ "$1" = "gcc" ] || [ "$1" = "clang" ] && $1 -Wall -O2 *.c -o output
    [ "$1" = "g++" ] || [ "$1" = "clang++" ] && $1 -Wall -O2 *.cpp -o output
}

lst() {
    for PKG in "$@"; do
        printf "Recommended packages for $PKG:\n" ; dnf rq -q --recommends $PKG
        printf "--------\n"
        printf "Required packages for $PKG:\n" ; dnf rq -q --requires $PKG
    done
}

pkg() {
    sudo dnf $1 ${@:2} && printf "$1 ${@:2}\n" >> $HOME/pkg_qt && rpm -qa | wc -l
}

musb(){
    lsblk
    read "DRIVE?Input drive to mount: "
    sudo mount /dev/$DRIVE /media/usb
    printf "Drive $DRIVE mounted.\n"
    read "OPT?CD into drive [y/N]: "
    [ "$OPT" = "y" ] || [ "$OPT" = "Y" ] && cd /media/usb
}

rusb(){
    read "OPT?Remove drive [y/N]: "
    [ "$OPT" = "y" ] || [ "$OPT" = "Y" ] && sudo umount /media/usb && printf "Drive unmounted.\n"
}

vim() {
    nvim $1
}

# EOF ----------------------------------------------------------------------------------------------
